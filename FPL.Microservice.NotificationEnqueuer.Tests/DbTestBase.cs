﻿namespace FPL.Microservice.NotificationEnqueuer.Tests
{
    using FPL.Common.DataAccess.DataContext;
    using FPL.Microservice.NotificationEnqueuer.DataAccess;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Moq.AutoMock;


    public abstract class DbTestBase<T> where T : class
    {
        protected readonly AutoMocker autoMocker;

        protected DbTestBase()
        {
            this.autoMocker = new AutoMocker();
        }

        protected T SubjectUnderTest => this.autoMocker.CreateInstance<T>();

        protected FPLDataContext GetDataContext()
        {
            var sqliteConnection = new SqliteConnection("Foreign Keys=False;DataSource=:memory:");
            sqliteConnection.Open();
            var dbContextOptions = new DbContextOptionsBuilder<FPLDataContext>()
                .UseSqlite(sqliteConnection)
                .Options;

            var context = new FPLDataContext(dbContextOptions);
            context.Database.EnsureCreated();
            return context;
        }
    }
}