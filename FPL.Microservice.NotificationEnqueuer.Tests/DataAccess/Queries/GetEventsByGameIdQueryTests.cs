﻿using FPL.Common.DataAccess.DataContext;
using FPL.Common.DataAccess.Entities;
using FPL.Microservice.NotificationEnqueuer.DataAccess;
using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Microservice.NotificationEnqueuer.Tests.DataAccess
{
    public class GetEventsByGameIdQueryTests : DbTestBase<GetEventsByGameIdQuery>
    {
        [Fact]
        public async Task ExecuteReturnsCorrectResults()
        {
            using(var context = GetDataContext())
            {
                var stubEvents = new List<Event>()
                {
                    new Event{ Key = 1, GameId = 2 },
                    new Event {Key = 2, GameId = 3}
                };

                context.AddRange(stubEvents);
                context.SaveChanges();
                this.autoMocker.GetMock<IDataContextFactory>()
                    .Setup(x => x.Create())
                    .Returns(context);

                var result = await SubjectUnderTest.Execute(2);

                Assert.Single(result);
                Assert.Equal(1, result.First().Key);
            }
        }
    }
}
