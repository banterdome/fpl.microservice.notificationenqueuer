﻿using FPL.Common.DataAccess.DataContext;
using FPL.Common.DataAccess.Entities;
using FPL.Microservice.NotificationEnqueuer.DataAccess;
using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Microservice.NotificationEnqueuer.Tests.DataAccess.Queries
{
    public class GetTokensByEventAndPlayerIdQueryTests : DbTestBase<GetTokensByEventAndPlayerIdQuery>
    {
        [Fact]
        public async Task ExecuteReturnsCorrectResults()
        {
            using (var context = GetDataContext())
            {
                var stubSubscription = new List<Subscription>()
                {
                    new Subscription() { Key = 1, Player = 1, Subscriber = 1},
                    new Subscription() { Key = 2, Player = 2, Subscriber = 2},
                    new Subscription() { Key = 3, Player = 3, Subscriber = 1},
                };

                var subscriptionType = new List<SubscriptionType>()
                {
                    new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 3, NotificationTypeKey = 2, SubscriptionKey = 3},
                    new SubscriptionType() { Key = 4, NotificationTypeKey = 2, SubscriptionKey = 2},
                };


                var tokens = new List<Token>()
                {
                    new Token { Key = 1, FirebaseToken = "Token1", LoginKey = 1},
                    new Token { Key = 2, FirebaseToken = "Token2", LoginKey = 2}
                };

                context.AddRange(subscriptionType);
                context.AddRange(stubSubscription);
                context.AddRange(tokens);

                context.SaveChanges();
                this.autoMocker.GetMock<IDataContextFactory>()
                    .Setup(x => x.Create())
                    .Returns(context);

                var result = await SubjectUnderTest.Execute(1, 1);

                Assert.Single(result);
                Assert.Equal(tokens[0].FirebaseToken, result[0]);
            }
        }

        [Fact]
        public async Task ExecuteReturnsSafelyIfNoTokensAreFound()
        {
            using (var context = GetDataContext())
            {
                var stubSubscription = new List<Subscription>()
                {
                    new Subscription() { Key = 1, Player = 1, Subscriber = 1},
                    new Subscription() { Key = 2, Player = 2, Subscriber = 2},
                    new Subscription() { Key = 3, Player = 3, Subscriber = 1},
                };

                var subscriptionType = new List<SubscriptionType>()
                {
                    new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 3, NotificationTypeKey = 2, SubscriptionKey = 3},
                    new SubscriptionType() { Key = 4, NotificationTypeKey = 2, SubscriptionKey = 2},
                };


                var tokens = new List<Token>()
                {
                    new Token { Key = 1, FirebaseToken = "Token1", LoginKey = 1},
                    new Token { Key = 2, FirebaseToken = "Token2", LoginKey = 2}
                };

                context.AddRange(subscriptionType);
                context.AddRange(stubSubscription);
                context.AddRange(tokens);

                context.SaveChanges();
                this.autoMocker.GetMock<IDataContextFactory>()
                    .Setup(x => x.Create())
                    .Returns(context);

                var result = await SubjectUnderTest.Execute(1, 3);

                Assert.Empty(result);
            }
        }
    }
}

