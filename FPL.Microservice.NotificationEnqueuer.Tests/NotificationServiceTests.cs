﻿using FPL.Common.Getters.Interfaces;
using FPL.Common.Models.FantasyPremierLeague;
using FPL.Microservice.NotificationEnqueuer;
using FPL.Microservice.NotificationEnqueuer.Business.Processors.Interfaces;
using FPL.Microservice.NotificationEnqueuer.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Moq.AutoMock;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Microservice.NotificationEnqueuer.Tests
{

    public class NotificationServiceTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task ServiceRunsCorrectlyWhenThereAreNoGamesToStartOrInPlay()
        {
            var stubInfo = new FantasyGeneralInfoModel() { Gameweeks = new List<FantasyGameweekModel>() { new FantasyGameweekModel { Id = 1, Current = true, Finished = false, Deadline = new DateTime(2021, 10, 10, 13, 00, 00) } } };
            var mockChannel = this.autoMocker.GetMock<IModel>();
            var logger = this.autoMocker.GetMock<ILogger<NotificationService>>();

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);
            this.autoMocker.GetMock<IThreadSleep>();
            this.autoMocker.GetMock<IGamesGetter>()
                .Setup(x => x.Get(1))
                .ReturnsAsync(new List<FantasyGameModel>() { new FantasyGameModel() { Started = true, FinishedProvisional = true } });

            this.autoMocker.GetMock<IDateTimeProvider>()
                .SetupSequence(x => x.Now())
                .Returns(new DateTime(2021, 10, 10, 12, 59, 00))
                .Returns(new DateTime(2021, 10, 10, 13, 59, 00))
                .Returns(new DateTime(2021, 10, 10, 14, 01, 00));

            var sut = this.autoMocker.CreateInstance<NotificationService>();

            await sut.Run(mockChannel.Object);

            this.autoMocker.GetMock<IThreadSleep>()
                .Verify(x => x.Sleep(60000), Times.Once);
            this.autoMocker.GetMock<IInPlayGameProcessor>()
                .Verify(x => x.Process(It.IsAny<FantasyGameModel>(), mockChannel.Object, stubInfo), Times.Never);
        }

        [Fact]
        public async Task ServiceRunsCorrectlyWhenThereAreGamesToStart()
        {
            var stubInfo = new FantasyGeneralInfoModel()
            {
                Gameweeks = new List<FantasyGameweekModel>()
                {
                    new FantasyGameweekModel { Id = 1, Current = true, Finished = true, Deadline = new DateTime(2021, 10, 10, 13, 00, 00) },
                    new FantasyGameweekModel { Id = 1, Current = false, Finished = false, Next = true, Deadline = new DateTime(2021, 10, 10, 13, 00, 00) }
                }
            };
            var mockChannel = this.autoMocker.GetMock<IModel>();
            var logger = this.autoMocker.GetMock<ILogger<NotificationService>>();

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);
            this.autoMocker.GetMock<IThreadSleep>();
            this.autoMocker.GetMock<IGamesGetter>()
                .SetupSequence(x => x.Get(1))
                .ReturnsAsync(new List<FantasyGameModel>() { new FantasyGameModel() { Started = false, FinishedProvisional = false, KickOffTime = new DateTime(2021, 10,10, 17, 00, 00) } })
                .ReturnsAsync(new List<FantasyGameModel>() { new FantasyGameModel() { Started = true, FinishedProvisional = false } })
                .ReturnsAsync(new List<FantasyGameModel>());

            this.autoMocker.GetMock<IDateTimeProvider>()
                .SetupSequence(x => x.Now())
                .Returns(new DateTime(2021, 10, 10, 12, 59, 00))
                .Returns(new DateTime(2021, 10, 10, 13, 59, 00))
                .Returns(new DateTime(2021, 10, 10, 14, 01, 00))
                .Returns(new DateTime(2021, 10, 10, 16, 57, 00))
                .Returns(new DateTime(2021, 10, 10, 16, 57, 00))
                .Returns(new DateTime(2021, 10, 10, 16, 58, 00))
                .Returns(new DateTime(2021, 10, 10, 17, 59, 00));

            var sut = this.autoMocker.CreateInstance<NotificationService>();

            var result = await sut.Run(mockChannel.Object);

            this.autoMocker.GetMock<IThreadSleep>()
                .Verify(x => x.Sleep(60000), Times.Once);
            this.autoMocker.GetMock<IThreadSleep>()
                .Verify(x => x.Sleep(120000), Times.Once);
            this.autoMocker.GetMock<IInPlayGameProcessor>()
                .Verify(x => x.Process(It.IsAny<FantasyGameModel>(), mockChannel.Object, stubInfo), Times.Once);
            Assert.Equal(0,result);
        }
    }
}
