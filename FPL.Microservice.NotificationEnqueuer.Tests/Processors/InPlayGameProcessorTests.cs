﻿using FPL.Common.DataAccess.Commands.Interfaces;
using FPL.Common.DataAccess.Entities;
using FPL.Common.Models.FantasyPremierLeague;
using FPL.Microservice.NotificationEnqueuer.Business.Processors;
using FPL.Microservice.NotificationEnqueuer.Business.Publishers;
using FPL.Microservice.NotificationEnqueuer.Configuration;
using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries.Interfaces;
using FPL.Microservice.NotificationEnqueuer.Messages;
using Moq;
using Moq.AutoMock;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Microsevice.NotificationEnqueur.Tests.Processors
{
    public class InPlayGameProcessorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task ProcessDoesNothingIfGameHasNoEvents()
        {
            var game = new FantasyGameModel() { Stats = new List<FantasyStatsModel>() };
            var stubChannel = this.autoMocker.GetMock<IModel>().Object;
            var stubQueueSettings = new QueueSettings("test", "test", "test", "test");

            this.autoMocker.Use(stubQueueSettings);

            var sut = this.autoMocker.CreateInstance<InPlayGameProcessor>();

            await sut.Process(game, stubChannel, new FantasyGeneralInfoModel());

            this.autoMocker.GetMock<IGetEventsByGameIdQuery>()
                .Verify(x => x.Execute(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public async Task ProcessDoesNotQueueEventsIfTheyAreAlreadyHandled()
        {
            var game = new FantasyGameModel()
            {
                Key = 1,
                Stats = new List<FantasyStatsModel>()
                {
                    new FantasyStatsModel()
                    {
                        Identifier = "goals_scored",
                        Home = new List<Value>
                        {
                            new Value() { Number = 1, Player = 2}
                        },
                        Away = new List<Value>()
                    }
                } 
            };

            this.autoMocker.GetMock<IGetEventsByGameIdQuery>()
                .Setup(x => x.Execute(game.Key))
                .ReturnsAsync(new List<Event>() { new Event { Player = 2, Amount = 1, GameId = 1, EventType = 1 } });

            var stubChannel = this.autoMocker.GetMock<IModel>().Object;
            var stubQueueSettings = new QueueSettings("test", "test", "test", "test");

            this.autoMocker.Use(stubQueueSettings);

            var sut = this.autoMocker.CreateInstance<InPlayGameProcessor>();

            await sut.Process(game, stubChannel, new FantasyGeneralInfoModel());

            this.autoMocker.GetMock<IInsertRangeCommand>()
                .Verify(x => x.Execute(It.IsAny<List<Event>>()), Times.Never);
        }

        [Fact]
        public async Task ProcessQueuesUnhandledEvents()
        {
            var firstNotificationMessage = new SendNotificationMessage();
            var secondNotifcationMessage = new SendNotificationMessage();

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel { Id = 2, Name = "Ronaldo" },
                    new FantasyPlayerModel { Id = 3, Name = "Henry" }
                },
                Teams = new List<FantasyTeamModel>
                {
                    new FantasyTeamModel{ Id =  1, Name = "Man Utd"},
                    new FantasyTeamModel { Id = 2, Name = "Arsenal"}
                }
            };

            var game = new FantasyGameModel()
            {
                Key = 1,
                HomeTeam = 1,
                AwayTeam = 2,
                HomeScore = 1,
                AwayScore = 0, 
                Stats = new List<FantasyStatsModel>()
                {
                    new FantasyStatsModel()
                    {
                        Identifier = "goals_scored",
                        Home = new List<Value>
                        {
                            new Value() { Number = 2, Player = 2}
                        },
                        Away = new List<Value>()
                    },
                    new FantasyStatsModel()
                    {
                        Identifier = "assists",
                        Away = new List<Value>
                        {
                            new Value() { Number = 1, Player = 3}
                        },
                        Home = new List<Value>()
                    }
                }
            };

            
            this.autoMocker.GetMock<IGetEventsByGameIdQuery>()
                .Setup(x => x.Execute(game.Key))
                .ReturnsAsync(new List<Event>() { new Event { Player = 2, Amount = 1, GameId = 1, EventType = 1 } });
            this.autoMocker.GetMock<IGetTokensByEventAndPlayerIdQuery>()
                .SetupSequence(x => x.Execute(2, 1))
                .ReturnsAsync(new List<string>() { "token" });
            this.autoMocker.GetMock<IGetTokensByEventAndPlayerIdQuery>()
                .SetupSequence(x => x.Execute(3, 2))
                .ReturnsAsync(new List<string>() { "token" });


            var stubChannel = this.autoMocker.GetMock<IModel>();
            var callsCount = 0;
            var stubQueueSettings = new QueueSettings("test", "testQueueName", "test", "test");
            this.autoMocker.GetMock<IMessagePublisher>()
                .Setup(x => x.Publish(stubChannel.Object, stubQueueSettings.QueueName, It.IsAny<ReadOnlyMemory<Byte>>()))
                .Callback<IModel, string, ReadOnlyMemory<Byte>>((channel, queuename, message) =>
                {
                    var decodedMessage = JsonConvert.DeserializeObject<SendNotificationMessage>(Encoding.UTF8.GetString(message.ToArray()));
                    if (callsCount++ == 0){
                        firstNotificationMessage = decodedMessage;
                    }
                    else
                    {
                        secondNotifcationMessage = decodedMessage;
                    }
                });;

            this.autoMocker.Use(stubQueueSettings);

            var sut = this.autoMocker.CreateInstance<InPlayGameProcessor>();

            await sut.Process(game, stubChannel.Object, stubInfo);

            this.autoMocker.GetMock<IMessagePublisher>()
                .Verify(x => x.Publish(stubChannel.Object, stubQueueSettings.QueueName, It.IsAny<ReadOnlyMemory<Byte>>()), Times.Exactly(2));

            Assert.Equal("Man Utd 1 - 0 Arsenal", firstNotificationMessage.Title);
            Assert.Equal("GOAL - Ronaldo", firstNotificationMessage.Body);
            Assert.Equal("ASSIST - Henry", secondNotifcationMessage.Body);
        }

        [Fact]
        public async Task ProcessRemovesEventFromDatabaseIfMessagePublicationFails()
        {
            var firstNotificationMessage = new SendNotificationMessage();
            var secondNotifcationMessage = new SendNotificationMessage();

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel { Id = 2, Name = "Ronaldo" },
                    new FantasyPlayerModel { Id = 3, Name = "Henry" }
                },
                Teams = new List<FantasyTeamModel>
                {
                    new FantasyTeamModel{ Id =  1, Name = "Man Utd"},
                    new FantasyTeamModel { Id = 2, Name = "Arsenal"}
                }
            };

            var game = new FantasyGameModel()
            {
                Key = 1,
                HomeTeam = 1,
                AwayTeam = 2,
                HomeScore = 1,
                AwayScore = 0,
                Stats = new List<FantasyStatsModel>()
                {
                    new FantasyStatsModel()
                    {
                        Identifier = "goals_scored",
                        Home = new List<Value>
                        {
                            new Value() { Number = 2, Player = 2}
                        },
                        Away = new List<Value>()
                    },
                    new FantasyStatsModel()
                    {
                        Identifier = "assists",
                        Away = new List<Value>
                        {
                            new Value() { Number = 1, Player = 3}
                        },
                        Home = new List<Value>()
                    }
                }
            };


            this.autoMocker.GetMock<IGetEventsByGameIdQuery>()
                .Setup(x => x.Execute(game.Key))
                .ReturnsAsync(new List<Event>() { new Event { Player = 2, Amount = 1, GameId = 1, EventType = 1 } });
            this.autoMocker.GetMock<IGetTokensByEventAndPlayerIdQuery>()
                .SetupSequence(x => x.Execute(2, 1))
                .ReturnsAsync(new List<string>() { "token" });
            this.autoMocker.GetMock<IGetTokensByEventAndPlayerIdQuery>()
                .SetupSequence(x => x.Execute(3, 2))
                .ReturnsAsync(new List<string>() { "token" });


            var stubChannel = this.autoMocker.GetMock<IModel>();
            var stubQueueSettings = new QueueSettings("test", "testQueueName", "test", "test");
            this.autoMocker.GetMock<IMessagePublisher>()
                .Setup(x => x.Publish(stubChannel.Object, stubQueueSettings.QueueName, It.IsAny<ReadOnlyMemory<Byte>>()))
                .Throws(new Exception("exception"));

            this.autoMocker.Use(stubQueueSettings);

            var sut = this.autoMocker.CreateInstance<InPlayGameProcessor>();

            await sut.Process(game, stubChannel.Object, stubInfo);

            this.autoMocker.GetMock<IDeleteEntityCommand>()
                .Verify(x => x.Execute(It.IsAny<Event>()), Times.Exactly(2));
        }
    }
}