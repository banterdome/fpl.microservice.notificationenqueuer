FROM mcr.microsoft.com/dotnet/sdk:5.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "FPL.Microservice.NotificationEnqueuer/FPL.Microservice.NotificationEnqueuer.csproj" -s "https://www.nuget.org/api/v2/" -s "https://gitlab.com/api/v4/projects/32083342/packages/nuget/index.json"
RUN dotnet restore "FPL.Microservice.NotificationEnqueuer.Tests/FPL.Microservice.NotificationEnqueuer.Tests.csproj" -s "https://www.nuget.org/api/v2/" 
#Run Tests
RUN dotnet test "FPL.Microservice.NotificationEnqueuer.Tests/FPL.Microservice.NotificationEnqueuer.Tests.csproj" /p:CollectCoverage=true
#Publish App
RUN dotnet publish "FPL.Microservice.NotificationEnqueuer/FPL.Microservice.NotificationEnqueuer.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:5.0  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "FPL.Microservice.NotificationEnqueuer.dll"]