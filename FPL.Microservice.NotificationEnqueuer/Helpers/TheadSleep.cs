﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Helpers
{
    public class TheadSleep : IThreadSleep
    {
        public void Sleep(int ms)
        {
            Thread.Sleep(ms);
        }
    }
}
