﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Messages
{
    public class SendNotificationMessage
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public List<string> Tokens { get; set; }
        public bool Retry { get; set; }
    }

}
