﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Configuration
{
    public record QueueSettings(string HostName, string QueueName, string Username, string Password)
    {
    }
}
