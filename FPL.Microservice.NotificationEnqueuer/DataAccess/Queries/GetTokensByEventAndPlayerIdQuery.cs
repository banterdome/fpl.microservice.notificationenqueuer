﻿namespace FPL.Microservice.NotificationEnqueuer.DataAccess.Queries
{
    using FPL.Common.DataAccess.DataContext;
    using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetTokensByEventAndPlayerIdQuery : IGetTokensByEventAndPlayerIdQuery
    {
        private readonly IDataContextFactory dataContextFactory;

        public GetTokensByEventAndPlayerIdQuery(IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task <List<string>> Execute(int playerId, int eventId)
        {
            using (var context = dataContextFactory.Create())
            {
                return 
                    await (from sub in context.Subscriptions
                            join subType in context.SubscriptionTypes on sub.Key equals subType.SubscriptionKey
                            join token in context.Tokens on sub.Subscriber equals token.LoginKey
                            where sub.Player == playerId && subType.NotificationTypeKey == eventId
                            select token.FirebaseToken).ToListAsync();
            }
        }
    }
}