﻿using FPL.Common.DataAccess.DataContext;
using FPL.Common.DataAccess.Entities;
using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.DataAccess.Queries
{
    public class GetEventsByGameIdQuery : IGetEventsByGameIdQuery
    {
        private readonly IDataContextFactory dataContextFactory;

        public GetEventsByGameIdQuery(IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task<List<Event>> Execute(int gameId)
        {
            using(var context = this.dataContextFactory.Create())
            {
                return await context.Events.Where(x => x.GameId == gameId).ToListAsync();
            }
        }
    }
}
