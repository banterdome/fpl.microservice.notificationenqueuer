﻿using FPL.Common.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.DataAccess.Queries.Interfaces
{
    public interface IGetEventsByGameIdQuery
    {
        Task<List<Event>> Execute(int gameId);
    }
}
