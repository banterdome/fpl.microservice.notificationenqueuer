﻿namespace FPL.Microservice.NotificationEnqueuer 
{
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using FPL.Microservice.NotificationEnqueuer.Configuration;
    using FPL.Microservice.NotificationEnqueuer.IoC;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    class Program
    {
        static async Task Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
               .UseServiceProviderFactory(new AutofacServiceProviderFactory())
               .ConfigureLogging((hostContext, logging) =>
               {
                   logging.SetMinimumLevel(LogLevel.Trace);
               })
               .ConfigureContainer<ContainerBuilder>((hostContext, builder) => {

                   var config = hostContext.Configuration;
                   builder.RegisterModule(new ServiceModule(config));
               })
               .ConfigureServices((hostContext, services) =>
               {
                   var configuration = hostContext.Configuration;

                   services.AddHostedService<Service>();
               })
               
               .Build();

            await host.RunAsync();
        }
    }
}
