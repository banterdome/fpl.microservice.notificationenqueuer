﻿using FPL.Common.Models.FantasyPremierLeague;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Business.Processors.Interfaces
{
    public interface IInPlayGameProcessor
    {
        Task Process(FantasyGameModel game, IModel channel, FantasyGeneralInfoModel info);
    }
}
