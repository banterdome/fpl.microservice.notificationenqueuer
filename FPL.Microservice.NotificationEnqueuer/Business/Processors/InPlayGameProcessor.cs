﻿using FPL.Common.DataAccess.Commands.Interfaces;
using FPL.Common.DataAccess.Entities;
using FPL.Common.Models.FantasyPremierLeague;
using FPL.Microservice.NotificationEnqueuer.Business.Processors.Interfaces;
using FPL.Microservice.NotificationEnqueuer.Business.Publishers;
using FPL.Microservice.NotificationEnqueuer.Configuration;
using FPL.Microservice.NotificationEnqueuer.DataAccess.Queries.Interfaces;
using FPL.Microservice.NotificationEnqueuer.Messages;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Business.Processors
{
    public class InPlayGameProcessor : IInPlayGameProcessor
    {
        private readonly IGetEventsByGameIdQuery getEventsByGameIdQuery;                 /* move into record param */
        private readonly IInsertRangeCommand insertCommand;
        private readonly IGetTokensByEventAndPlayerIdQuery getTokensByEventAndPlayerIdQuery;
        private readonly QueueSettings queueSettings;
        private readonly ILogger<InPlayGameProcessor> logger;
        private readonly IDeleteEntityCommand deleteEntityCommand;
        private readonly IMessagePublisher messagePublisher;

        public InPlayGameProcessor(
            IGetEventsByGameIdQuery getEventsByGameIdQuery, IInsertRangeCommand insertCommand, IGetTokensByEventAndPlayerIdQuery getTokensByEventAndPlayerIdQuery, QueueSettings queueSettings, 
            ILogger<InPlayGameProcessor> logger, IDeleteEntityCommand deleteEntityCommand, IMessagePublisher messagePublisher)
        {
            this.getEventsByGameIdQuery = getEventsByGameIdQuery;
            this.insertCommand = insertCommand;
            this.getTokensByEventAndPlayerIdQuery = getTokensByEventAndPlayerIdQuery;
            this.queueSettings = queueSettings;
            this.logger = logger;
            this.deleteEntityCommand = deleteEntityCommand;
            this.messagePublisher = messagePublisher;
        }

        public async Task Process(FantasyGameModel game, IModel channel, FantasyGeneralInfoModel info)
        {
            logger.LogInformation($"Handling game {game.Key}");

            //get events, use automapper to refactor this
            var goals = new List<Value>();
            var assists = new List<Value>();

            goals.AddRange(game.Stats.Where(x => x.Identifier == "goals_scored").SelectMany(x => x.Home).ToList());
            goals.AddRange(game.Stats.Where(x => x.Identifier == "goals_scored").SelectMany(x => x.Away).ToList());
            assists.AddRange(game.Stats.Where(x => x.Identifier == "assists").SelectMany(x => x.Home).ToList());
            assists.AddRange(game.Stats.Where(x => x.Identifier == "assists").SelectMany(x => x.Away).ToList());

            if (goals.Count > 0 || assists.Count > 0)
            {
                logger.LogInformation($"Game {game.Key} has {goals.Count} to handle");
                //get handled events for this game
                var handledEvents = await this.getEventsByGameIdQuery.Execute(game.Key);

                //get unhandledEvents
                var unhandledEvents = await GetUnhandledEvents(goals, 1, handledEvents, game);   //move event type to enum
                unhandledEvents.AddRange(await GetUnhandledEvents(assists, 2, handledEvents, game)); //move event type to enum

                logger.LogInformation($"There are {unhandledEvents.Count} for game {game.Key}");
                if (unhandledEvents.Count == 0)
                {
                    return;
                }

                //add events to db
                await this.insertCommand.Execute(unhandledEvents);

                foreach(var unhandledEvent in unhandledEvents.Where(x => x.Tokens.Count > 0))
                {
                    //queue message, remove from DB if it fails
                    try
                    {
                        var message = new SendNotificationMessage
                        {
                            Title = GetTitle(game, info),
                            Body = GetBody(unhandledEvent, info),
                            Retry = true,
                            Tokens = unhandledEvent.Tokens
                        };

                        logger.LogInformation($"Publishing message with body {message.Body}");
                        this.messagePublisher.Publish(channel, this.queueSettings.QueueName, Encoding.Default.GetBytes(JsonConvert.SerializeObject(message)));
                    }
                    catch(Exception e)
                    {
                        this.logger.LogError(e.Message);
                        this.logger.LogError($"Failed to queue message for event {unhandledEvent.Key} from game {game.Key}, removing from DB");
                        await this.deleteEntityCommand.Execute(unhandledEvent);
                    }
                }             
            }
        }

        private string GetTitle(FantasyGameModel game, FantasyGeneralInfoModel info)
        {
            return $"{info.Teams.Where(x => x.Id == game.HomeTeam).First().Name} {game.HomeScore} - {game.AwayScore} {info.Teams.Where(x => x.Id == game.AwayTeam).First().Name}";
        }

        private string GetBody(Event pointsEvent, FantasyGeneralInfoModel info)
        {
            return $"{(pointsEvent.EventType == 1 ? "GOAL" : "ASSIST")} - {info.Players.Where(x => x.Id == pointsEvent.Player).First().Name}";
        }

        private async Task<List<Event>> GetUnhandledEvents(List<Value> events, int eventType, List<Event> handledEvents, FantasyGameModel game)
        {
            var unhandledEvents = new List<Event>();

            foreach (var e in events)
            {
                if (!handledEvents.Exists(x => x.Player == e.Player && x.Amount == e.Number && x.EventType == eventType))
                {
                    unhandledEvents.Add(new Event
                    {
                        Player = e.Player,
                        Amount = e.Number,
                        EventType = eventType,
                        GameId = game.Key,
                        Tokens = await this.getTokensByEventAndPlayerIdQuery.Execute(e.Player, eventType)
                    });
                }
            }

            return unhandledEvents;
        }
    }
}