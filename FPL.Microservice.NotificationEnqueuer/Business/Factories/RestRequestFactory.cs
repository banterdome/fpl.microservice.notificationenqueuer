﻿namespace FPL.Microservice.NotificationEnqueuer.Business.Factories
{
    using FPL.Microservice.NotificationEnqueuer.Business.Factories.Interfaces;
    using RestSharp;
    
    public class RestRequestFactory : IRestRequestFactory
    {
        public IRestRequest Create(string endpoint)
        {
            return new RestRequest(endpoint, Method.GET);
        }
    }
}
