﻿namespace FPL.Microservice.NotificationEnqueuer.Business.Factories
{
    using FPL.Microservice.NotificationEnqueuer.Business.Factories.Interfaces;
    using RestSharp;

    public class RestClientFactory : IRestClientFactory
    {
        public IRestClient Create(string url)
        {
            return new RestClient(url);
        }
    }
}
