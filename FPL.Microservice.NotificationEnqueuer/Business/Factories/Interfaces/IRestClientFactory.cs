﻿namespace FPL.Microservice.NotificationEnqueuer.Business.Factories.Interfaces
{
    using RestSharp;

    public interface IRestClientFactory
    {
        IRestClient Create(string URL);
    }
}
