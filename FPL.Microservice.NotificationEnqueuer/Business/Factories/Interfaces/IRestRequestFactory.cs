﻿namespace FPL.Microservice.NotificationEnqueuer.Business.Factories.Interfaces
{
    using RestSharp;

    public interface IRestRequestFactory
    {
        IRestRequest Create(string endpoint);
    }
}
