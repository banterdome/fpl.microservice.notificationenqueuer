﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Business.Publishers
{
    public class MessagePublisher : IMessagePublisher
    {
        public void Publish(IModel channel, string queueName, ReadOnlyMemory<byte> body)
        {
            channel.BasicPublish(exchange: "", routingKey: queueName, basicProperties: channel.CreateBasicProperties(), body: body);
        }
    }
}
