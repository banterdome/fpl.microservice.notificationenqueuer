﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer.Business.Publishers
{
    public interface IMessagePublisher
    {
        void Publish(IModel channel, string queueName, ReadOnlyMemory<Byte> body);
    }
}
