﻿namespace FPL.Microservice.NotificationEnqueuer
{
    using FPL.Common.Getters.Interfaces;
    using FPL.Common.Models.FantasyPremierLeague;
    using FPL.Microservice.NotificationEnqueuer.Business.Processors.Interfaces;
    using FPL.Microservice.NotificationEnqueuer.Configuration;
    using FPL.Microservice.NotificationEnqueuer.Helpers;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using RabbitMQ.Client;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class NotificationService : INotificationService
    {
        private readonly IGeneralInfoGetter generalInfoGetter;
        private readonly ILogger<NotificationService> logger;
        private readonly IGamesGetter gamesGetter;
        private readonly IInPlayGameProcessor inPlayGameProcessor;
        private readonly IThreadSleep threadSleep;
        private readonly IDateTimeProvider dateTimeProvider;

        public NotificationService(IGeneralInfoGetter generalInfoGetter, ILogger<NotificationService> logger, IGamesGetter gamesGetter, IInPlayGameProcessor inPlayGameProcessor, IThreadSleep threadSleep, 
            IDateTimeProvider dateTimeProvider)
        {
            this.generalInfoGetter = generalInfoGetter;
            this.logger = logger;
            this.gamesGetter = gamesGetter;
            this.inPlayGameProcessor = inPlayGameProcessor;
            this.threadSleep = threadSleep;
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task<int> Run(IModel channel)
        {
            logger.LogInformation("Getting general info from FPL");
            
            var info = await this.generalInfoGetter.GetGeneralInfo();
            var gameweek = info.CurrentGameweek;

            if (gameweek.Finished)
            {
                gameweek = info.Gameweeks.Where(x => x.Next).FirstOrDefault();
            }

            //deadline is -1 hour, plus we don't care about the time before games starting.
            var deadline = gameweek.Deadline.AddHours(1); //need to take into account BST here

            while (deadline > this.dateTimeProvider.Now())
            {
                logger.LogInformation($"It is before the deadline, sleeping until {deadline}");
                SleepUntil(deadline);
            }

            logger.LogInformation($"Deadline has passed, awoke at {this.dateTimeProvider.Now()} getting games");
            var inPlayGames = new List<FantasyGameModel>();
            var gamesToStart = new List<FantasyGameModel>();

            FilteredGames(ref inPlayGames, ref gamesToStart, gameweek.Id);
           
            while(inPlayGames.Count > 0 || gamesToStart.Count > 0)
            {
                if(inPlayGames.Count > 0)
                {
                    //time how long it takes to process, sleep for 1 minute?

                    logger.LogInformation($"Found {inPlayGames.Count} in play games");

                    var tasks = new List<Task>();
                    foreach(var game in inPlayGames)
                    {
                        tasks.Add(this.inPlayGameProcessor.Process(game, channel, info));
                    }

                    await Task.WhenAll(tasks);
                }


                if(gamesToStart.Count > 0 && inPlayGames.Count == 0)
                {
                    //sleep until games start
                    var kickOffTime = gamesToStart.OrderBy(x => x.KickOffTime).First().KickOffTime;
                    while(kickOffTime > this.dateTimeProvider.Now())
                    {
                        logger.LogInformation($"No games are currently being played, sleeping until {kickOffTime}");
                        SleepUntil(kickOffTime);
                    }
                }


                FilteredGames(ref inPlayGames, ref gamesToStart, gameweek.Id);
            }

            logger.LogInformation("All games are finished");
            return 0;
        }

        private void FilteredGames(ref List<FantasyGameModel> inPlayGames, ref List<FantasyGameModel> gamesToStart, int gameweekId)
        {
            var games = this.gamesGetter.Get(gameweekId).Result;

            inPlayGames = games.Where(x => x.Started && !x.FinishedProvisional).ToList();
            gamesToStart = games.Where(x => !x.Started).ToList();
        }

        private void SleepUntil(DateTime time)
        {
            var now = this.dateTimeProvider.Now();
            TimeSpan span = time - now;
            int msUntilTime = (int)span.TotalMilliseconds;

            this.threadSleep.Sleep(msUntilTime);
        }
    }
}
