﻿using RabbitMQ.Client;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer
{
    public interface INotificationService
    {
        Task<int> Run(IModel channel);
    }
}
