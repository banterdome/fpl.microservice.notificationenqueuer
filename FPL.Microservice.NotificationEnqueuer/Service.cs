﻿using FPL.Microservice.NotificationEnqueuer.Configuration;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using System.Threading;
using System.Threading.Tasks;

namespace FPL.Microservice.NotificationEnqueuer
{
    public class Service : BackgroundService
    {
        private readonly QueueSettings queueSettings;
        private readonly INotificationService service;

        public Service(QueueSettings queueSettings, INotificationService service)
        {
            this.queueSettings = queueSettings;
            this.service = service;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var factory = new ConnectionFactory() { HostName = queueSettings.HostName, UserName = queueSettings.Username, Password = queueSettings.Password };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueSettings.QueueName,
                                            durable: false,
                                            exclusive: false,
                                            autoDelete: false,
                                            arguments: null);

                    await service.Run(channel);
                }
            };
        }
    }
}
