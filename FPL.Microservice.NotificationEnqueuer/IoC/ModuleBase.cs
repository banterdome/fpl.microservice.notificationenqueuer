﻿namespace FPL.Microservice.NotificationEnqueuer.IoC
{
    using Autofac;
    using Microsoft.Extensions.Configuration;
    using System.IO;
    using System.Reflection;
    using Microsoft.Extensions.Configuration.Json;
    using Microsoft.Extensions.Logging;

    public class ModuleBase : Autofac.Module
    {
        protected readonly IConfiguration Configuration;

        public ModuleBase(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        { 
            base.Load(builder);
            var assembly = Assembly.GetExecutingAssembly();
            var fplCommonAssembly = Assembly.Load("FPL.Common");

            

            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Getter")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Factory")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Processor")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Query")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Command")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Sleep")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Provider")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("Publisher")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(assembly).Where(t => t.Name.EndsWith("NotificationService")).AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(fplCommonAssembly).Where(t => t.Name.EndsWith("Factory")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(fplCommonAssembly).Where(t => t.Name.EndsWith("Getter")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(fplCommonAssembly).Where(t => t.Name.EndsWith("Command")).AsImplementedInterfaces();
           
        }
    }
}
