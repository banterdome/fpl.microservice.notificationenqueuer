﻿namespace FPL.Microservice.NotificationEnqueuer.IoC
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using Autofac;
    using FPL.Common.Settings;
    using FPL.Microservice.NotificationEnqueuer.Configuration;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    [ExcludeFromCodeCoverage]
    public class ServiceModule : ModuleBase
    {
        public ServiceModule(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            
            base.Load(builder);
            builder.Register(this.GetFantasyPremierLeagueSettings)
               .SingleInstance();

            builder.Register(this.GetDatabaseSettings)
                .SingleInstance();
            builder.Register(this.GetQueueSettings)
                .SingleInstance();
        }

        private QueueSettings GetQueueSettings(IComponentContext context)
        {
            return new QueueSettings
            (
                Configuration.GetValue<string>("QueueSettings:HostName"),
                Configuration.GetValue<string>("QueueSettings:QueueName"),
                Configuration.GetValue<string>("QueueSettings:Username"),
                Configuration.GetValue<string>("QueueSettings:Password")
            );
        }

        private FantasyPremierLeagueSettings GetFantasyPremierLeagueSettings(IComponentContext context)
        {
            return new FantasyPremierLeagueSettings(Configuration.GetSection("FantasyPremierLeagueSettings:FplApiUrl").Value);
        }
        
        private DatabaseSettings GetDatabaseSettings(IComponentContext context)
        {
            return new DatabaseSettings(Configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
        }
    }
}